<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class RoleController extends Controller
{
    public function index()
    {
        $roles = User::all();

        return Inertia::render(
            'Admin/Roles',
            ['roles' => $roles]
        );
    }

    public function edit(User $post)
    {
        // return Inertia::render('Admin/Posts/EditPost', ['post' => $post]);
    }

    // public function update(Request $request, User $post)
    // {
    //     // $request->validate([
    //     //     'title' => 'required|string|max:255',
    //     //     'content' => 'required',
    //     // ]);

    //     // $post->title = $request->title;
    //     // $post->content = $request->content;
    //     // $post->save();
    //     // sleep(1);

    //     // return redirect()->route('posts.index')->with('message', 'Post Updated Successfully');

    // }

    public function update(Request $request)
    {
        Validator::make($request->all(), [
            'role' => ['required'],
        ])->validate();

        if ($request->has('id')) {
            User::find($request->input('id'))->update($request->all());
            return redirect()->back()
                    ->with('message', 'Post Updated Successfully.');
        }

    }

    public function destroy(User $role)
    {
        $role->delete();
        sleep(1);

        return redirect()->route('roles.index')->with('message', 'Post Delete Successfully');
    }
}
