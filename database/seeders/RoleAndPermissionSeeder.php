<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\MOdels\Role;
use Spatie\Permission\MOdels\Permission;

class RoleAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $role = Role::where('name', 'user')->first();
        $role->givePermissionTo('manage blog');

        $role = Role::where('name', 'admin')->first();
        $role->givePermissionTo('manage blog');
        $role->givePermissionTo('admin');
    }
}
